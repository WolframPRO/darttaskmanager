//
//  LoginViewController.swift
//  DartTaskManager
//
//  Created by Владимир on 23/12/2018.
//  Copyright © 2018 варя. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var logoOffsetY: NSLayoutConstraint!
    @IBOutlet weak var loginIndicator: UIActivityIndicatorView!
    @IBOutlet weak var enterButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loginIndicator.stopAnimating()
        loginAndPasswordFields(hide: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.logoOffsetY.constant = 0
        self.view.layoutIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let userDefaults = UserDefaults.standard
        if let login = userDefaults.string(forKey: "Login"), let password = userDefaults.string(forKey: "Password"){
            self.signIn(login: login, password: password)
            self.loginIndicator.startAnimating()
        } else {
            self.logoOffsetY.constant = -150
            UIView.animate(withDuration: 0.35, delay: 0.15, options: .curveEaseIn, animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
            UIView.animate(withDuration: 0.15, delay: 0.35, options: .allowUserInteraction, animations: {
                self.loginAndPasswordFields(hide: false)
            }, completion: nil)
        }
    }

    @IBAction func enterAction(_ sender: Any) {
        guard let login = loginField.text else {
            return
        }
        guard let password = passwordField.text else {
            return
        }
        signIn(login: login, password: password)
    }
    
    //MARK: - Helpers
    
    fileprivate func loginAndPasswordFields(hide: Bool) {
        self.loginField.isEnabled = !hide
        self.passwordField.isEnabled = !hide
        self.enterButton.isEnabled = !hide
        if hide {
            self.loginField.alpha = 0
            self.passwordField.alpha = 0
            self.enterButton.alpha = 0
        } else {
            self.loginField.alpha = 1
            self.passwordField.alpha = 1
            self.enterButton.alpha = 1
        }
        
    }
    
    func signIn(login: String, password: String) {
        self.loginIndicator.startAnimating()
        self.loginAndPasswordFields(hide: true)
        Authentication.signIn(login: login, password: password, completion: { errorMessage in
            self.loginAndPasswordFields(hide: true)
            self.loginIndicator.stopAnimating()
            if let errorMessage = errorMessage {
                DispatchQueue.main.async {
                    self.showShortAlert(message: errorMessage)
                    UIView.animate(withDuration: 0.3, delay: 0.35, options: .allowUserInteraction, animations: {
                        self.loginAndPasswordFields(hide: false)
                    }, completion: nil)
                    let userDefaults = UserDefaults.standard
                    userDefaults.removeObject(forKey: "Login")
                    userDefaults.removeObject(forKey: "Password")
                }
                return
            } else {
                let userDefaults = UserDefaults.standard
                userDefaults.set(login, forKey: "Login")
                userDefaults.set(password, forKey: "Password")
                self.performSegue(withIdentifier: "loginSegue", sender: nil)
            }
        })
    }
    
    func showShortAlert(message: String) {
        let alertController = UIAlertController(title: message, message:
            nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        self.present(alertController, animated: true, completion: nil)
        let _ = Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (_) in
            alertController.dismiss(animated: true, completion: nil)
        })
    }

}

extension LoginViewController: UITextFieldDelegate {
    
}

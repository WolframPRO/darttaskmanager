//
//  ContactListCell.swift
//  DartTaskManager
//
//  Created by варя on 24/12/2018.
//  Copyright © 2018 варя. All rights reserved.
//

import UIKit

class ContactListCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var postLabel: UILabel!
    @IBOutlet weak var uIdLabel: UILabel!
    
    var user: User!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func set(model: User) {
        self.user = model
        let family = model.family != nil ? model.family! + " " : ""
        let name = model.name != nil ? model.name! + " " : ""
        let patronymic = model.patronymic != nil ? model.patronymic! + " " : ""
        
        self.nameLabel.text = family + name + patronymic
        self.postLabel.text = model.position ?? ""
        self.uIdLabel.text = model.uid ?? ""
    }
}

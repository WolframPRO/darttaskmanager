//
//  DetailTaskViewController.swift
//  DartTaskManager
//
//  Created by Владимир on 24/12/2018.
//  Copyright © 2018 варя. All rights reserved.
//

import UIKit

class DetailTaskViewController: UITableViewController {

    public var task: OneTaskModel!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var createdDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var priorityLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var workerLabel: UILabel!
    
    @IBOutlet weak var firstCommentTextView: UITextView!
    @IBOutlet weak var commentTextField: UITextView!
    @IBOutlet weak var lastCommentAutor: UILabel!
    @IBOutlet weak var lastCommentDate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = UITableView.automaticDimension
        definesPresentationContext = true
        
        set(task: task)
    }
    
    func set(task: OneTaskModel) {
        titleLabel.text = task.task.title ?? "Задача"
        idLabel.text = "id: " + String(task.task.id ?? 0)
        statusLabel.text = task.status.title ?? "Не удалось получить статус"
        timeLabel.text = String(task.lastMessage.spendedTime ?? 0) + "/" + String(task.firstMessage.planTime ?? 0)
        priorityLabel.text = task.lastMessage.getPriority
        authorLabel.text = task.firstMessage.authorUid ?? "Нет автора"
        workerLabel.text = task.lastMessage.userUid ?? "Нет исполнителя"
        
        firstCommentTextView.text = task.firstMessage.text ?? ""
        commentTextField.text = task.lastMessage.text ?? ""
        lastCommentAutor.text = task.lastMessage.authorUid
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "dd MMMM YYYY"
        if let date = task.lastMessage.date  {
            let convertLastTime = Date(milliseconds: Double(date))
            lastCommentDate.text = dateFormatter.string(from: convertLastTime)
        }
        if let createDate = task.firstMessage.date {
            let convertStartTime = Date(milliseconds: Double(createDate))
            createdDateLabel.text = dateFormatter.string(from: convertStartTime)
        }
        if let endDate = task.firstMessage.endDate {
            let convertEndTime = Date(milliseconds: Double(endDate))
            endDateLabel.text = dateFormatter.string(from: convertEndTime)
        }
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

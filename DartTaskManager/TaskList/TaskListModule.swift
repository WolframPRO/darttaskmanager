//
//  TaskListModule.swift
//  DartTaskManager
//
//  Created by Владимир on 24/12/2018.
//Copyright © 2018 варя. All rights reserved.
//

import UIKit

//MARK: TaskListModule Class
final class TaskListModule: BaseModule {
}

//MARK: - TaskListModule Components
extension TaskListModule {

    var presenter: TaskListPresenterProtocol {
        return self._presenter as! TaskListPresenterProtocol
    }
    var interactor: TaskListInteractorProtocol {
        return self._interactor as! TaskListInteractorProtocol
    }
    var view: TaskListViewProtocol {
        return self._view as! TaskListViewProtocol
    }
    var router: TaskListRouterProtocol {
        return self._router as! TaskListRouterProtocol
    }
    var tableViewModel: TaskListTableViewModel {
        return self._tableViewModel as! TaskListTableViewModel
    }
}

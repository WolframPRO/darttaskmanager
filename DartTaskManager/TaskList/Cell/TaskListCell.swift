//
//  TaskListCell.swift
//  DartTaskManager
//
//  Created by Владимир on 24/12/2018.
//  Copyright © 2018 варя. All rights reserved.
//

import UIKit

class TaskListCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var priorityLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var createdDateLabel: UILabel!
    
    var taskModel: OneTaskModel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

    
    public func set(model: OneTaskModel){
        self.taskModel = model
        self.nameLabel.text = model.task.title ?? ""
        self.statusLabel.text = model.status.title ?? ""
        self.priorityLabel.text = model.lastMessage.getPriority
        self.idLabel.text = String(model.task.id ?? 0)
        
        self.createdDateLabel.text = "Дата создания: " + Date.convertData(fromUNIXdata: model.firstMessage.date ?? 0)
        guard let endDate = model.firstMessage.endDate else {
            return
        }
        self.createdDateLabel.text = self.createdDateLabel.text! + "\nСрок исполнения: " + Date.convertData(fromUNIXdata: endDate)
    }

}

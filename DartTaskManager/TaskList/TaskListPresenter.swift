//
//  TaskListPresenter.swift
//  DartTaskManager
//
//  Created by Владимир on 24/12/2018.
//Copyright © 2018 варя. All rights reserved.
//

import Foundation

// MARK: - TaskListPresenter Class
final class TaskListPresenter: BasePresenter {
    
    override func viewIsAboutToAppear() {
        self.reloadData()
    }
    
    func reloadData() {
        interactor.reloadData {
            self.tableViewModel.setTaskList(taskList: self.interactor.getData())
            self.view.table.reloadData()
        }
    }
}

// MARK: - TaskListPresenter Protocol
extension TaskListPresenter: TaskListPresenterProtocol {
    func reloadData(completion: @escaping () -> ()) {
        interactor.reloadData {
            self.tableViewModel.setTaskList(taskList: self.interactor.getData())
            completion()
        }
    }
    
    func select(task: OneTaskModel) {
        let detailViewController = router.goToTaskDetail()
        detailViewController.task = task
    }
    
}

// MARK: - TaskList Viper Components
private extension TaskListPresenter {
    var view: TaskListViewProtocol {
        return module.view
    }
    var interactor: TaskListInteractorProtocol {
        return module.interactor
    }
    var router: TaskListRouterProtocol {
        return module.router
    }
    var module: TaskListModule {
        return _module as! TaskListModule
    }
    var tableViewModel: TaskListTableViewModel{
        return module.tableViewModel
    }
}

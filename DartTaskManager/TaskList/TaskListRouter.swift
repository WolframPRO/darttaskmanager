//
//  TaskListRouter.swift
//  DartTaskManager
//
//  Created by Владимир on 24/12/2018.
//Copyright © 2018 варя. All rights reserved.
//

import Foundation
import UIKit

// MARK: - TaskListRouter class
final class TaskListRouter: BaseRouter {
}

// MARK: - TaskListRouter Protocol
extension TaskListRouter: TaskListRouterProtocol {
    func goToTaskDetail() -> DetailTaskViewController {
        let sb = UIStoryboard.init(name: "TaskList", bundle: .main)
        let vc = sb.instantiateViewController(withIdentifier: "detailTask") as! DetailTaskViewController
        self.view.show(vc, sender: nil)
        return vc
    }
    
}

// MARK: - TaskList Viper Components
private extension TaskListRouter {
    var module: TaskListModule {
        return _module as! TaskListModule
    }
    var presenter: TaskListPresenterProtocol {
        return module.presenter
    }
}

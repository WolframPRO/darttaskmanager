//
//  DetailContactTableViewController.swift
//  DartTaskManager
//
//  Created by варя on 24/12/2018.
//  Copyright © 2018 варя. All rights reserved.
//

import UIKit
import MessageUI

class DetailContactTableViewController: UITableViewController, MFMailComposeViewControllerDelegate {

    public var user: User!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 3
        case 1:
            if user.email != nil {
                return 1
            } else {
                return 0
            }
        case 2:
            return user.phone != nil ? user.phone!.count : 0
        default:
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Сотрудник"
        case 1:
            return "eMail"
        case 2:
            return "Телефоны"
        default:
            return ""
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let family = user.family != nil ? user.family! + " " : ""
                let name = user.name != nil ? user.name! + " " : ""
                let patronymic = user.patronymic != nil ? user.patronymic! + " " : ""

                let fullName = family + name + patronymic
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "Subtitle", for: indexPath) as! SubtitleTableViewCell
                cell.primaryText = fullName
                cell.detailText = user.uid
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "RightDetail", for: indexPath) as! RightDetailTableViewCell
                cell.primaryText = "Дата рождения:"
                cell.detailText = user.birthday
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "RightDetail", for: indexPath) as! RightDetailTableViewCell
                cell.primaryText = "Должность:"
                cell.detailText = user.position
                return cell
            }
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Email", for: indexPath) as! EmailTableViewCell
            cell.email = user.email ?? ""
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Phone", for: indexPath) as! PhoneTableViewCell
            let phones = user.phone?.allObjects
            let phone = phones?[indexPath.row] as! Phone
            cell.phone = phone.phone ?? ""
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let configurableCell = cell as! Configurable
        configurableCell.configure()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 {
            let phone = user.phone?.allObjects[indexPath.row] as! Phone
            let phoneNum = phone.phone != nil ? phone.phone! : ""
            guard let number = URL(string: "tel://+7" + phoneNum) else { return }
            UIApplication.shared.open(number)
        } else if indexPath.section == 1 {
            let mailComposeVC = configureMailComposer()
            guard let email = user.email else {
                return
            }
            
            mailComposeVC.setToRecipients([email])
            
            if MFMailComposeViewController.canSendMail() {
                self.present(mailComposeVC, animated: true, completion: nil)
            } else {
                showMailError()
            }
        }
    }
    
    // кому отправить добить
    func configureMailComposer() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        
        mailComposerVC.setSubject("")
        mailComposerVC.setMessageBody("", isHTML: false)
        
        return mailComposerVC
    }
    
    func showMailError() {
        let alert = UIAlertController(title: "Невозможно выслать email", message: "Ваше устройство не может высылать email-сообщения", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(dismiss)
        self.present(alert, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

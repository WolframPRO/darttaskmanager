//
//  SubtitleTableViewCell.swift
//  DartTaskManager
//
//  Created by варя on 24/12/2018.
//  Copyright © 2018 варя. All rights reserved.
//

import UIKit

class SubtitleTableViewCell: UITableViewCell, Configurable {

    @IBOutlet weak var primaryLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!

    var primaryText: String!
    var detailText: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func set(primary: String, detailed: String) {
        primaryText = primary
        detailText = detailed
    }
    
    func configure() {
        primaryLabel.text = primaryText
        detailLabel.text = detailText
    }
}

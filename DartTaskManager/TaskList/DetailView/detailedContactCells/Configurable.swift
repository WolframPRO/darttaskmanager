//
//  Configurable.swift
//  DartTaskManager
//
//  Created by варя on 24/12/2018.
//  Copyright © 2018 варя. All rights reserved.
//

protocol Configurable {
    func configure()
}

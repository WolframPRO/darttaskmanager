//
//  EmailTableViewCell.swift
//  DartTaskManager
//
//  Created by варя on 24/12/2018.
//  Copyright © 2018 варя. All rights reserved.
//

import UIKit

class EmailTableViewCell: UITableViewCell, Configurable {

    @IBOutlet weak var emailLabel: UILabel!
    
    var email: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func set(_ str: String) {
        email = str
    }
    
    func configure() {
        emailLabel.text = email
    }
}

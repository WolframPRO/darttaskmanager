//
//  PhoneTableViewCell.swift
//  DartTaskManager
//
//  Created by варя on 24/12/2018.
//  Copyright © 2018 варя. All rights reserved.
//

import UIKit

class PhoneTableViewCell: UITableViewCell, Configurable {

    @IBOutlet weak var phoneLabel: UILabel!

    var phone: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func set(_ str: String) {
        phone = str
    }

    func configure() {
        phoneLabel.text = phone
    }
}

//
//  TaskListTableViewModel.swift
//  DartTaskManager
//
//  Created by Владимир on 24/12/2018.
//Copyright © 2018 варя. All rights reserved.
//

import Foundation
import UIKit

// MARK: - TaskListDisplayData class
final class TaskListTableViewModel: BaseTableViewModel {
    var taskList: [OneTaskModel] = []
    var filteredTaskList: [OneTaskModel] = []
    
    var lowPriorityTaskList: [OneTaskModel] = []
    var mediumPriorityTaskList: [OneTaskModel] = []
    var heightPriorityTaskList: [OneTaskModel] = []
    
    var filterON = false
    
    func setTaskList(taskList: [OneTaskModel]) {
        self.taskList = taskList
        self.mediumPriorityTaskList = []
        self.heightPriorityTaskList = []
        self.lowPriorityTaskList = []
        for task in taskList {
            switch task.lastMessage.priorityId {
            case 2: mediumPriorityTaskList.append(task)
            case 3: heightPriorityTaskList.append(task)
            default: lowPriorityTaskList.append(task)
            }
        }
    }
    //MARK: DataSource
    override func number(ofRowsInSection section: Int, tableView: UITableView) -> Int {
        guard !filterON else {
            return filteredTaskList.count
        }
        switch section {
        case 0:
            return heightPriorityTaskList.count
        case 1:
            return mediumPriorityTaskList.count
        default:
            return lowPriorityTaskList.count
        }
    }
    override func cell(forRow row: Int, section: Int, tableView: UITableView) -> UITableViewCell {
        guard !filterON else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell") as! TaskListCell
            cell.set(model: filteredTaskList[row])
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell") as! TaskListCell
        if section == 0 {
            cell.set(model: heightPriorityTaskList[row])
        } else if section == 1 {
            cell.set(model: mediumPriorityTaskList[row])
        } else if section == 2 {
            cell.set(model: lowPriorityTaskList[row])
        }
        return cell
    }
    override func number(ofSections tableView: UITableView) -> Int {
        guard !filterON else {
            return 1
        }
        return 3
    }
    //MARK: Delegate
    override func did(selectRow row: Int, section: Int, tableView: UITableView) {
        guard !filterON else {
            presenter.select(task: filteredTaskList[row])
            return
        }
        
        if section == 0 {
            presenter.select(task:heightPriorityTaskList[row])
        } else if section == 1 {
            presenter.select(task: mediumPriorityTaskList[row])
        } else if section == 2 {
            presenter.select(task: lowPriorityTaskList[row])
        }
        
    }
    //override func height(forRow row: Int, section: Int, tableView: UITableView) -> CGFloat {
    //    return UITableView.automaticDimension
    //}
    
    override func height(forHeader section: Int, tableView: UITableView) -> CGFloat {
        guard !filterON else {
            return 0
        }
        if section == 0 && heightPriorityTaskList.count > 0 {
            return 20
        } else if section == 1 && mediumPriorityTaskList.count > 0 {
            return 20
        } else if section == 2 && lowPriorityTaskList.count > 0 {
            return 20
        }
        return 0
    }
    
    override func height(forFooter section: Int, tableView: UITableView) -> CGFloat {
        return 0
    }
    
    override func titleForHeader(inSection section: Int) -> String? {
        guard !filterON else {
            return nil
        }
        if section == 0 && heightPriorityTaskList.count > 0 {
            return "Высокий приоритет"
        } else if section == 1 && mediumPriorityTaskList.count > 0 {
            return "Средний приоритет"
        } else if section == 2 && lowPriorityTaskList.count > 0 {
            return "Низкий приоритет"
        }
        return nil
    }
    
    
    //MARK: - searchResultUpdate
    override func update(searchResult text: String) {
        if text.count > 0 {
            filterON = true
            filteredTaskList = taskList.filter({ (task) -> Bool in
                if task.task.title!.contains(text) {
                    return true
                }
                if task.lastMessage.getPriority == text {
                    return true
                }
                if String(task.task.id!).contains(text) {
                    return true
                }
                return false
            })
        } else {
            filterON = false
            filteredTaskList = []
        }
    }
}

private extension TaskListTableViewModel {
    var presenter: TaskListPresenter{
        return (_module._presenter as! TaskListPresenter)
    }
}

//
//  TaskListModuleProtocols.swift
//  DartTaskManager
//
//  Created by Владимир on 24/12/2018.
//Copyright © 2018 варя. All rights reserved.
//
import UIKit

//MARK: - TaskListRouter Protocol
protocol TaskListRouterProtocol: BaseRouterProtocol {
    func goToTaskDetail() -> DetailTaskViewController
}

//MARK: - TaskListView Protocol
protocol TaskListViewProtocol: BaseViewProtocol {
    var table: UITableView {get}
}

//MARK: - TaskListPresenter Protocol
protocol TaskListPresenterProtocol: BasePresenterProtocol {
    func reloadData(completion: @escaping () -> ())
    func select(task: OneTaskModel)
}

//MARK: - TaskListInteractor Protocol
protocol TaskListInteractorProtocol: BaseInteractorProtocol {
    func reloadData(completion: @escaping () -> ())
    func getData() -> [OneTaskModel]
}

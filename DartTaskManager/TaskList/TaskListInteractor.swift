//
//  TaskListInteractor.swift
//  DartTaskManager
//
//  Created by Владимир on 24/12/2018.
//Copyright © 2018 варя. All rights reserved.
//

import Foundation
import CoreData
// MARK: - TaskListInteractor Class
final class TaskListInteractor: BaseInteractor {
    var taskList: [OneTaskModel] = []
}

// MARK: - ___VARIABLE_ViperitModuleName___Interactor Protocol
extension TaskListInteractor: TaskListInteractorProtocol {
    func reloadData(completion: @escaping () -> ()) {
        TaskList.getTaskList(completion: {taskList in
            self.taskList = taskList
            completion()
        })
    }
    
    func getData() -> [OneTaskModel] {
        return taskList
    }
    
}

// MARK: - Interactor Viper Components Protocol
private extension TaskListInteractor {
    var presenter: TaskListPresenterProtocol {
        return module.presenter
    }
    var module: TaskListModule {
        return _module as! TaskListModule
    }
}

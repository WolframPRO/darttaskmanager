//
//  TaskListView.swift
//  DartTaskManager
//
//  Created by Владимир on 24/12/2018.
//Copyright © 2018 варя. All rights reserved.
//

import UIKit

//MARK: TaskListView Class
final class TaskListView: BaseTableView {
    let searchController = UISearchController(searchResultsController: nil)
    
    @IBOutlet weak var refreshDataControl: UIRefreshControl!
    
    override func viewDidLoad() {
        let module = AppModules.TaskList.build()
        self._module = module
        super.viewDidLoad()
        module.change(view: self)
        
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = true
        
        searchController.searchResultsUpdater = self
        
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Начните поиск"
        definesPresentationContext = true
        
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = UITableView.automaticDimension
    }
    
    @IBAction func refreshData(_ sender: Any) {
        presenter.reloadData {
            self.refreshDataControl.endRefreshing()
            let _ = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { (_) in
                self.tableView.reloadSections([0,1,2], with: .automatic)
            })
        }
    }
}

//MARK: - TaskListView Protocol
extension TaskListView: TaskListViewProtocol {
    var table: UITableView {
        return self.tableView
    }
    
}

// MARK: - TaskListView Viper Components Protocol
private extension TaskListView {
    var module: TaskListModule {
        return _module as! TaskListModule
    }
    var presenter: TaskListPresenterProtocol {
        return module.presenter
    }
}

extension TaskListView: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText =  searchController.searchBar.text else {
            return
        }
        tableVM.update(searchResult: searchText)
        table.reloadData()
    }
}

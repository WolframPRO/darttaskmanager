//
//  SettingsView.swift
//  DartTaskManager
//
//  Created by Владимир on 25/12/2018.
//  Copyright © 2018 варя. All rights reserved.
//

import UIKit

class SettingsView: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    @IBAction func exitAction(_ sender: Any) {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: "Login")
        userDefaults.removeObject(forKey: "Password")
        self.navigationController?.tabBarController?.dismiss(animated: true, completion: nil)
    }

}

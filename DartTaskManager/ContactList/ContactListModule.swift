//
//  ContactListModule.swift
//  DartTaskManager
//
//  Created by Владимир on 24/12/2018.
//Copyright © 2018 варя. All rights reserved.
//

import UIKit

//MARK: ContactListModule Class
final class ContactListModule: BaseModule {
}

//MARK: - ContactListModule Components
extension ContactListModule {

    var presenter: ContactListPresenterProtocol {
        return self._presenter as! ContactListPresenterProtocol
    }
    var interactor: ContactListInteractorProtocol {
        return self._interactor as! ContactListInteractorProtocol
    }
    var view: ContactListViewProtocol {
        return self._view as! ContactListViewProtocol
    }
    var router: ContactListRouterProtocol {
        return self._router as! ContactListRouterProtocol
    }
    var tableViewModel: ContactListTableViewModel {
        return self._tableViewModel as! ContactListTableViewModel
    }
}

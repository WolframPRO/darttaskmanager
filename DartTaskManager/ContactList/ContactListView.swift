//
//  ContactListView.swift
//  DartTaskManager
//
//  Created by Владимир on 24/12/2018.
//Copyright © 2018 варя. All rights reserved.
//

import UIKit

//MARK: ContactListView Class
final class ContactListView: BaseTableView {
    let searchController = UISearchController(searchResultsController: nil)
    @IBOutlet weak var refreshDataControl: UIRefreshControl!
    
    override func viewDidLoad() {
        let module = AppModules.ContactList.build()
        self._module = module
        super.viewDidLoad()
        module.change(view: self)
        
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = true
        
        searchController.searchResultsUpdater = self
        
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Начните поиск"
        definesPresentationContext = true
        
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = UITableView.automaticDimension
    }
    
    @IBAction func refreshData(_ sender: Any) {
        presenter.reloadData {
            self.refreshDataControl.endRefreshing()
            self.tableView.reloadData()
        }
    }
}



//MARK: - ContactListView Protocol
extension ContactListView: ContactListViewProtocol {
    var table: UITableView {
        return self.tableView
    }
    
}

// MARK: - ContactListView Viper Components Protocol
private extension ContactListView {
    var module: ContactListModule {
        return _module as! ContactListModule
    }
    var presenter: ContactListPresenterProtocol {
        return module.presenter
    }
}

extension ContactListView: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText =  searchController.searchBar.text else {
            return
        }
        tableVM.update(searchResult: searchText)
        table.reloadData()
    }
}

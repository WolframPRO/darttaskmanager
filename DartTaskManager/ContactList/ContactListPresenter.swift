//
//  ContactListPresenter.swift
//  DartTaskManager
//
//  Created by Владимир on 24/12/2018.
//Copyright © 2018 варя. All rights reserved.
//

import Foundation

// MARK: - ContactListPresenter Class
final class ContactListPresenter: BasePresenter {
    
    override func viewIsAboutToAppear() {
        self.reloadData()
    }

    func reloadData() {
        interactor.reloadDataFromLocal()
        
        if self.interactor.getDepartmentData().count == 0 {
            interactor.reloadData {
                self.tableViewModel.setDepartmentList(depList: self.interactor.getDepartmentData(), usList: self.interactor.getUserList())
                self.view.table.reloadData()
            }
        } else {
            self.tableViewModel.setDepartmentList(depList: self.interactor.getDepartmentData(), usList: self.interactor.getUserList())
            self.view.table.reloadData()
        }
    }
}

// MARK: - ContactListPresenter Protocol
extension ContactListPresenter: ContactListPresenterProtocol {
    func reloadData(completion: @escaping () -> ()) {
        interactor.reloadData {
            self.tableViewModel.setDepartmentList(depList: self.interactor.getDepartmentData(), usList: self.interactor.getUserList())
            completion()
        }
    }
    
    func select(user: User) {
        let detailViewController = router.goToUserDetail()
        detailViewController.user = user
    }
}

// MARK: - ContactList Viper Components
private extension ContactListPresenter {
    var view: ContactListViewProtocol {
        return module.view
    }
    var interactor: ContactListInteractorProtocol {
        return module.interactor
    }
    var router: ContactListRouterProtocol {
        return module.router
    }
    var module: ContactListModule {
        return _module as! ContactListModule
    }
    var tableViewModel: ContactListTableViewModel{
        return module.tableViewModel
    }
}

//
//  ContactListRouter.swift
//  DartTaskManager
//
//  Created by Владимир on 24/12/2018.
//Copyright © 2018 варя. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ContactListRouter class
final class ContactListRouter: BaseRouter {
}

// MARK: - ContactListRouter Protocol
extension ContactListRouter: ContactListRouterProtocol {
    func goToUserDetail() -> DetailContactTableViewController {
        let sb = UIStoryboard.init(name: "ContactList", bundle: .main)
        let vc = sb.instantiateViewController(withIdentifier: "detailUser") as! DetailContactTableViewController
        self.view.show(vc, sender: nil)
        return vc
    }
    
}

// MARK: - ContactList Viper Components
private extension ContactListRouter {
    var module: ContactListModule {
        return _module as! ContactListModule
    }
    var presenter: ContactListPresenterProtocol {
        return module.presenter
    }
}

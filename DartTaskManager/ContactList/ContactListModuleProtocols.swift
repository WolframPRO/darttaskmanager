//
//  ContactListModuleProtocols.swift
//  DartTaskManager
//
//  Created by Владимир on 24/12/2018.
//Copyright © 2018 варя. All rights reserved.
//

import UIKit

//MARK: - ContactListRouter Protocol
protocol ContactListRouterProtocol: BaseRouterProtocol {
    func goToUserDetail() -> DetailContactTableViewController
}

//MARK: - ContactListView Protocol
protocol ContactListViewProtocol: BaseViewProtocol {
    var table: UITableView {get}
}

//MARK: - ContactListPresenter Protocol
protocol ContactListPresenterProtocol: BasePresenterProtocol {
    func select(user: User)
    func reloadData(completion: @escaping () -> ())

}

//MARK: - ContactListInteractor Protocol
protocol ContactListInteractorProtocol: BaseInteractorProtocol {
    func reloadData(completion: @escaping () -> ())
    func reloadDataFromLocal()
    func getDepartmentData() -> [Departament]
    func getUserList() -> [User]
}

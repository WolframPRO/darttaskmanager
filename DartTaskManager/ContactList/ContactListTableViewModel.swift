//
//  ContactListTableViewModel.swift
//  DartTaskManager
//
//  Created by Владимир on 24/12/2018.
//Copyright © 2018 варя. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ContactListDisplayData class
final class ContactListTableViewModel: BaseTableViewModel {
    var departamentList: [Departament] = []
    var userList: [User] = []
    var filteredUserList: [User] = []

    var filterON = false
    
    
    func setDepartmentList(depList: [Departament], usList: [User]) {
        self.departamentList = depList
        self.userList = usList
        self.filteredUserList = usList
        filterON = false
    }
    
    //MARK: DataSource
    override func number(ofRowsInSection section: Int, tableView: UITableView) -> Int {
        guard !filterON else {
            return filteredUserList.count
        }
        return departamentList[section].users?.count ?? 0
    }
    
    override func cell(forRow row: Int, section: Int, tableView: UITableView) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell") as! ContactListCell

        guard !filterON else {
            cell.set(model: filteredUserList[row])
            return cell
        }
        
        cell.set(model: departamentList[section].users!.allObjects[row] as! User)
        
        return cell
    }
    
    override func number(ofSections tableView: UITableView) -> Int {
        guard !filterON else {
            return 1
        }
        return departamentList.count
    }
    
    //MARK: Delegate
    override func did(selectRow row: Int, section: Int, tableView: UITableView) {
        guard !filterON else {
            presenter.select(user: filteredUserList[row])
            return
        }
        
        presenter.select(user: departamentList[section].users!.allObjects[row] as! User)
    }
    
    override func height(forHeader section: Int, tableView: UITableView) -> CGFloat {
        guard !filterON else {
            return 0
        }
        
        if section == 0 {
            return 0
        }
        
        return 30
    }
    
    override func height(forFooter section: Int, tableView: UITableView) -> CGFloat {
        return 0
    }
    
    override func titleForHeader(inSection section: Int) -> String? {
        guard !filterON else {
            return nil
        }
        
        return departamentList[section].name ?? "Департамент"
    }
    
    
    //MARK: - searchResultUpdate
    override func update(searchResult text: String) {
        if text.count > 0 {
            filterON = true
            filteredUserList = userList.filter({ (user) -> Bool in
                let family = user.family != nil ? user.family! + " " : ""
                let name = user.name != nil ? user.name! + " " : ""
                let patronymic = user.patronymic != nil ? user.patronymic! + " " : ""
                
                let fullName = family + name + patronymic
                
                if fullName.contains(text) {
                    return true
                }
                
                if user.uid != nil && user.uid!.contains(text) {
                    return true
                }
                
                if user.position != nil && user.position!.contains(text) {
                    return true
                }
                
                if user.departament != nil && user.departament!.name != nil && user.departament!.name!.contains(text) {
                    return true
                }
                
                return false
            })
        } else {
            filterON = false
            filteredUserList = []
        }
    }
}


private extension ContactListTableViewModel {
    var presenter: ContactListPresenter{
        return (_module._presenter as! ContactListPresenter)
    }
}

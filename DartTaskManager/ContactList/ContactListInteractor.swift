//
//  ContactListInteractor.swift
//  DartTaskManager
//
//  Created by Владимир on 24/12/2018.
//Copyright © 2018 варя. All rights reserved.
//

import Foundation
import CoreData

// MARK: - ContactListInteractor Class
final class ContactListInteractor: BaseInteractor {
    var departamentList: [Departament] = []
}

// MARK: - ___VARIABLE_ViperitModuleName___Interactor Protocol
extension ContactListInteractor: ContactListInteractorProtocol {
    func reloadData(completion: @escaping () -> ()) {
        Contacts.getContacts(forced: false) {[unowned self] (usr, dep) in
            CoreDataConverter.convertAndSave(departaments: dep, users: usr)
            let request: NSFetchRequest<Departament> = Departament.fetchRequest()
            do{
                let departments: [Departament] = try PersistenceService.context.fetch(request)
                let filteredDepartments = departments.filter({ department -> Bool in
                    guard let users = department.users else {
                        return false
                    }
                    return users.count > 0
                })
                self.departamentList = filteredDepartments
                completion()
            } catch {
                
            }
        }
    }
    
    func reloadDataFromLocal() {
        let request: NSFetchRequest<Departament> = Departament.fetchRequest()
        do{
            let departments: [Departament] = try PersistenceService.context.fetch(request)
            let filteredDepartments = departments.filter({ department -> Bool in
                guard let users = department.users else {
                    return false
                }
                return users.count > 0
            })
            self.departamentList = filteredDepartments
        } catch {
            
        }
    }
    
    func getUserList() -> [User] {
        var users: [User] = []
        for dep in departamentList {
            users.append(contentsOf: dep.users?.allObjects as! [User])
        }
        return users
    }
    
    func getDepartmentData() -> [Departament] {
        return departamentList
    }
    
}

// MARK: - Interactor Viper Components Protocol
private extension ContactListInteractor {
    var presenter: ContactListPresenterProtocol {
        return module.presenter
    }
    var module: ContactListModule {
        return _module as! ContactListModule
    }
}

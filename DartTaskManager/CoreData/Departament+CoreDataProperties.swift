//
//  Departament+CoreDataProperties.swift
//  DartTaskManager
//
//  Created by Владимир on 24/12/2018.
//  Copyright © 2018 варя. All rights reserved.
//
//

import Foundation
import CoreData


extension Departament {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Departament> {
        return NSFetchRequest<Departament>(entityName: "Departament")
    }

    @NSManaged public var cn: String?
    @NSManaged public var name: String?
    @NSManaged public var users: NSSet?

}

// MARK: Generated accessors for users
extension Departament {

    @objc(addUsersObject:)
    @NSManaged public func addToUsers(_ value: User)

    @objc(removeUsersObject:)
    @NSManaged public func removeFromUsers(_ value: User)

    @objc(addUsers:)
    @NSManaged public func addToUsers(_ values: NSSet)

    @objc(removeUsers:)
    @NSManaged public func removeFromUsers(_ values: NSSet)

}

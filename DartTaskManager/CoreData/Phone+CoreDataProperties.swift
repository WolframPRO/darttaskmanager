//
//  Phone+CoreDataProperties.swift
//  DartTaskManager
//
//  Created by Владимир on 24/12/2018.
//  Copyright © 2018 варя. All rights reserved.
//
//

import Foundation
import CoreData


extension Phone {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Phone> {
        return NSFetchRequest<Phone>(entityName: "Phone")
    }

    @NSManaged public var phone: String?
    @NSManaged public var user: User?

}

//
//  User+CoreDataProperties.swift
//  DartTaskManager
//
//  Created by Владимир on 24/12/2018.
//  Copyright © 2018 варя. All rights reserved.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var name: String?
    @NSManaged public var family: String?
    @NSManaged public var patronymic: String?
    @NSManaged public var email: String?
    @NSManaged public var birthday: String?
    @NSManaged public var position: String?
    @NSManaged public var active: Bool
    @NSManaged public var uid: String?
    @NSManaged public var phone: NSSet?
    @NSManaged public var departament: Departament?

}

// MARK: Generated accessors for phone
extension User {

    @objc(addPhoneObject:)
    @NSManaged public func addToPhone(_ value: Phone)

    @objc(removePhoneObject:)
    @NSManaged public func removeFromPhone(_ value: Phone)

    @objc(addPhone:)
    @NSManaged public func addToPhone(_ values: NSSet)

    @objc(removePhone:)
    @NSManaged public func removeFromPhone(_ values: NSSet)

}

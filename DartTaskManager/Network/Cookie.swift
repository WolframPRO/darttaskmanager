//
//  Cookie.swift
//  DartTaskManager
//
//  Created by варя on 25/10/2018.
//  Copyright © 2018 варя. All rights reserved.
//

import UIKit
import Alamofire

class Cookie {
    static let timeForCookieSaving = TimeInterval(3600 * 24 * 30) // 30 days
    
    static func saveCookieFrom(response: DataResponse<Any>) {
        guard let headerFields = response.response?.allHeaderFields as? [String: String] else { return }
        let url = response.response?.url
        let cookies = HTTPCookie.cookies(withResponseHeaderFields: headerFields, for: url!)
        for cookie in cookies {
            var cookieProperties = [HTTPCookiePropertyKey: AnyObject]()
            cookieProperties[HTTPCookiePropertyKey.name] = cookie.name as AnyObject
            cookieProperties[HTTPCookiePropertyKey.value] = cookie.value as AnyObject
            cookieProperties[HTTPCookiePropertyKey.domain] = cookie.domain as AnyObject
            cookieProperties[HTTPCookiePropertyKey.path] = cookie.path as AnyObject
            cookieProperties[HTTPCookiePropertyKey.version] = NSNumber(value: cookie.version)
            cookieProperties[HTTPCookiePropertyKey.expires] = NSDate().addingTimeInterval(Cookie.timeForCookieSaving)
            let newCookie = HTTPCookie(properties: cookieProperties)
            HTTPCookieStorage.shared.setCookie(newCookie!)
        }
    }
}

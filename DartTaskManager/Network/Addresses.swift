//
//  Addresses.swift
//  DartTaskManager
//
//  Created by варя on 24/10/2018.
//  Copyright © 2018 варя. All rights reserved.
//

import UIKit

class Addresses {
    static let globalUrl = "http://localhost:8080/"
    
    // MARK: - Authentication
    static let authenticateUrl = globalUrl + "login"
    
    // MARK: - Contacts
    static let getContactsUrl = globalUrl + "user/contacts"
    
    // MARK: - TaskList
    static let getTaskListUrl = globalUrl + "task/list"
}

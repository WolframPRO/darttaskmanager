//
//  BaseRequest.swift
//  DartTaskManager
//
//  Created by варя on 30/10/2018.
//  Copyright © 2018 варя. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class BaseRequest {
    static let headers = ["Content-Type": "application/x-www-form-urlencoded"]
    
    static func netError(throwedError: Error) {
        print("net error")
    }
    
    
    static func request(url:URLConvertible, useResult: @escaping (JSON, DataResponse<Any>) -> Void) {
        manager.request(url, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let result = JSON(value) 
                useResult(result, response)
            case .failure(let error):
                netError(throwedError: error)
            }
        }
        
    }
    
    static func request(url:URLConvertible, body:Dictionary<String, Any>, useResult: @escaping (JSON, DataResponse<Any>) -> Void) {
        
        manager.request(url,
                method: .post,
                parameters: body,
                encoding: URLEncoding.httpBody,
                headers: headers
            ).validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let result = JSON(value)
                    useResult(result, response)
                case .failure(let error):
                    netError(throwedError: error)
                    let result = JSON(error)
                    useResult(result, response)
                }
        }
    }
    
    static func request(url:URLConvertible, query:Dictionary<String, Any>, useResult: @escaping (JSON, DataResponse<Any>) -> Void) {
        manager.request(url,
                parameters: query,
                encoding: URLEncoding(destination: .queryString), headers: headers
            ).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let result = JSON(value)
                    useResult(result, response)
                case .failure(let error):
                    netError(throwedError: error)
                }
        }
    }
}

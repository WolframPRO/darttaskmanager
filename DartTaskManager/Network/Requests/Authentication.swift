//
//  SignIn.swift
//  DartTaskManager
//
//  Created by варя on 24/10/2018.
//  Copyright © 2018 варя. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
 
class Authentication {
    static func signIn(login:String, password:String, completion: @escaping (_ errorMessage: String?)->()) {
        let url = Addresses.authenticateUrl
        let body = [
            "login" : login,
            "password" : password
        ]
        
        BaseRequest.request(url: url, body: body) {result, response in
            if (result["error"]).boolValue {
                if let errorMessage = result["errorMessage"].string {
                    completion(errorMessage)
                    return
                }
                completion(nil)
            } else {
                Cookie.saveCookieFrom(response: response)
                completion(nil)
            }
        }
    }
}

//
//  GetContacts.swift
//  DartTaskManager
//
//  Created by варя on 24/10/2018.
//  Copyright © 2018 варя. All rights reserved.
//

import UIKit
import Cache

class Contacts {
    let usersStorage: Storage<[UserModel]>?
    let departmentsStorage: Storage<[DepartmentModel]>?
    
    private let usersKey = "lastUsers"
    private let departmentsKey = "lastDepartments"
    
    static let shared: Contacts = Contacts()
    private init() {
        let diskConfig = DiskConfig(name: "INNER_STORAGE_Contacts_Disk",
                                    expiry: .date(Date().addingTimeInterval(2*3600)),
                                    maxSize: 10000,
                                    protectionType: .complete)
        
        let memoryConfig = MemoryConfig(expiry: .never, countLimit: 200, totalCostLimit: 0)
        
        self.usersStorage = try? Storage(
            diskConfig: diskConfig,
            memoryConfig: memoryConfig,
            transformer: TransformerFactory.forCodable(ofType: [UserModel].self)
        )
        
        self.departmentsStorage = try? Storage(
            diskConfig: diskConfig,
            memoryConfig: memoryConfig,
            transformer: TransformerFactory.forCodable(ofType: [DepartmentModel].self)
        )

    }
    
    static func getContacts(forced: Bool, completion: @escaping (([UserModel], [DepartmentModel])) -> ()) {
        Contacts.shared.getContacts(forced: forced, completion: completion)
    }
    
    func getFromCache() -> ([UserModel]?, [DepartmentModel]?) {
        guard let usersStorage = self.usersStorage, let departmentsStorage = self.departmentsStorage else {
            return (nil, nil)
        }
        let users = try? usersStorage.object(forKey: self.usersKey)
        let departments = try? departmentsStorage.object(forKey: self.departmentsKey)
        return (users, departments)
    }
    
    func getContacts(forced: Bool, completion: @escaping (([UserModel], [DepartmentModel])) -> ()) {
        let cashed = getFromCache()
        if let users = cashed.0, let departments = cashed.1 {
            completion((users, departments))
        }
        
        let url = Addresses.getContactsUrl
        
        BaseRequest.request(url: url) { result, _ in
            var userList: [UserModel] = []
            var departamentList: [DepartmentModel] = []
            if (result["error"]).boolValue {
                // TODO: ошибка с данными (неверный логин или что-то еще, в ошибке есть описание)
            } else {
                let decoder = JSONDecoder()
                
                for task in result["answer"]["users"] {
                    do {
                        let user = try decoder.decode(UserModel.self, from: (task.1.rawData()))
                        userList.append(user)
                    } catch {}
                }
                
                for departament in result["answer"]["departaments"] {
                    do {
                        let departament = try decoder.decode(DepartmentModel.self, from: (departament.1.rawData()))
                        if let users = departament.users {
                            if users.count > 0 {
                                departamentList.append(departament)
                            }
                        }
                    } catch {}
                }
                
                try? self.usersStorage?.setObject(userList, forKey: self.usersKey)
                try? self.departmentsStorage?.setObject(departamentList, forKey: self.departmentsKey)
            }
            completion((userList, departamentList))
        }
    }
    
}

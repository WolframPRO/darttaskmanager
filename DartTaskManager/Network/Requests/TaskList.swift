//
//  TaskList.swift
//  DartTaskManager
//
//  Created by варя on 24/10/2018.
//  Copyright © 2018 варя. All rights reserved.
//

import UIKit
import SwiftyJSON

class TaskList {

    static func getTaskList(completion: @escaping ([OneTaskModel]) -> ()) {
        let url = Addresses.getTaskListUrl
        BaseRequest.request(url: url) { result, response in
            var taskList: [OneTaskModel] = []
            if (result["error"]).boolValue {
                // TODO: ошибка с данными (неверный логин или что-то еще, в ошибке есть описание)
            } else {
                //((response.result.value as! [String: Any?])["tasks"] as! [Any])
                let decoder = JSONDecoder()
                for task in result["tasks"] {
                    do {
                        let oneTask = try decoder.decode(OneTaskModel.self, from: (task.1.rawData()))
                        taskList.append(oneTask)
                    } catch {
                        
                    }
                }
            }
            completion(taskList)
        }
    }
    
}

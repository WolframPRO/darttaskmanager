//
//  Priorities.swift
//  DartTaskManager
//
//  Created by варя on 23/12/2018.
//  Copyright © 2018 варя. All rights reserved.
//

enum Priority: Int {
    case low = 1
    case medium = 2
    case high = 3
}

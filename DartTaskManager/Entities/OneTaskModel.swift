//
//  OneTask.swift
//  DartTaskManager
//
//  Created by варя on 23/12/2018.
//  Copyright © 2018 варя. All rights reserved.
//
import Foundation

struct tasks: Codable {
    var tasks: [OneTaskModel]
}
struct OneTaskModel: Codable {
    var status: StatusTast
    var task: Task
    var firstMessage: Message
    var lastMessage: Message
}

struct StatusTast: Codable {
    var title: String?
    var code: String?
    var id: Int?
}

struct Task: Codable {
    var orderId: Int?
    var isNotEdit: Int?
    var isAnalytics: Int?
    var reasonId: Int?
    var title: String?
    var typeId: Int?
    var isMagent: Int?
    var date: Int?
    var errTpId: Int?
    var taskType: Int?
    var projectId: Int?
    var parentTaskId: Int?
    var id: Int?
    var requestId: Int?
    var consType: Int?
    var isMlk: Int?
    var errRegId: Int?
}

struct Message: Codable {
    var priorityId:Int?
    var date: Int?
    var text: String?
    var spendedTime: Int?
    var taskStatus: String?
    var authorUid: String?
    var endDate: Int?
    var statusId: Int?
    var planTime: Int?
    var addAddresses: String?
    var id: Int?
    var userUid: String?
    
    var getPriority: String {
        guard let priorityId = self.priorityId else {
            return "Не определено"
        }
        switch priorityId {
            case 2: return "Средний"
            case 3: return "Высокий"
            default: return "Низкий"
        }
    }
}

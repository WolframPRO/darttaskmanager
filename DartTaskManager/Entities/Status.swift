//
//  Status.swift
//  DartTaskManager
//
//  Created by варя on 23/12/2018.
//  Copyright © 2018 варя. All rights reserved.
//

enum Status: Int {
    case NEW = 1
    case IN_PROCESS = 2
    case IN_TEST = 3
}

//class Status {
//    let code = ""
//    let id = 0
//    let title = ""
//}

//
//  User.swift
//  DartTaskManager
//
//  Created by варя on 25/10/2018.
//  Copyright © 2018 варя. All rights reserved.
//
import Foundation

struct UserModel: Codable {
    let uid: String?
    let name: String?
    let family: String?
    let patronymic: String?
    let phones: [String]?
    let email: String?
    let birthday: String?
    let position: String?
    let active: Bool?
}

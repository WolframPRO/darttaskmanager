//
//  Department.swift
//  DartTaskManager
//
//  Created by варя on 25/10/2018.
//  Copyright © 2018 варя. All rights reserved.
//

struct DepartmentModel: Codable {
    let cn: String?
    let name: String?
    let users: [String]?
}

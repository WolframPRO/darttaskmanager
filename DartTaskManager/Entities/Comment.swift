//
//  Comment.swift
//  DartTaskManager
//
//  Created by варя on 23/12/2018.
//  Copyright © 2018 варя. All rights reserved.
//
import Foundation

class Comment {
    let id = 0
    let date = Date()
    let text = ""
    
    let userUid = ""
    let status = Status.NEW
    
    let authorUid = ""
    let endDate: Date? = nil
    
    let priotity = Priority.low
    
    let spendedTime = 0
    let planTime = 0
    let addAddressed = ""
    let taskStatus = ""
}

//
//  Date.swift
//  DartTaskManager
//
//  Created by Владимир on 24/12/2018.
//  Copyright © 2018 варя. All rights reserved.
//

import Foundation

extension Date {
    init(milliseconds:Double) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
    
    static func convertData(fromUNIXdata: Int) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        let convertLastTime = Date(milliseconds: Double(fromUNIXdata))
        dateFormatter.dateFormat = "dd MMMM YYYY"
        return dateFormatter.string(from: convertLastTime)
    }
}

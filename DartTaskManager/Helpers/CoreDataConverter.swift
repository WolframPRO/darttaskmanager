//
//  CoreDataConverter.swift
//  DartTaskManager
//
//  Created by Владимир on 24/12/2018.
//  Copyright © 2018 варя. All rights reserved.
//

import Foundation

class CoreDataConverter {

    static func convertAndSave(departaments: [DepartmentModel], users: [UserModel]) {
        PersistenceService.resetDatabase()
        var coreDataUsers: [User] = []
        for usr in users {
            let user = User.init(context: PersistenceService.context)
            user.name = usr.name
            user.family = usr.family
            user.patronymic = usr.patronymic
            user.email = usr.email
            user.birthday = usr.birthday
            user.position = usr.position
            user.active = usr.active ?? false
            user.uid = usr.uid
            
            if let phones = usr.phones {
                for phn in phones {
                    let phone = Phone.init(context: PersistenceService.context)
                    phone.phone = phn
                    phone.user = user
                    user.addToPhone(phone)
                }
            }
            coreDataUsers.append(user)
        }
        
        for dep in departaments {
            let coreDataDepartment = Departament.init(context: PersistenceService.context)
            coreDataDepartment.cn = dep.cn
            coreDataDepartment.name = dep.name
            
            
            let usersInDepartment = coreDataUsers.filter { (userTemp) -> Bool in
                guard let uid = userTemp.uid else {
                    return false
                }
                guard let users = dep.users else {
                    return false
                }
                if users.contains(uid) {
                    return true
                }
                return false
            }
            
            coreDataDepartment.addToUsers(NSSet.init(array: usersInDepartment))
        }
        PersistenceService.saveContext()
    }
}

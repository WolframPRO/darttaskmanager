parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"
cp Podfile temp_podfile
rm Podfile
sed -e '/gitlab/d' temp_podfile > Podfile
echo '********** Удаление подов от gitlab **********'
pod install
rm Podfile
cp temp_podfile Podfile
rm temp_podfile
echo '********** Установка подов из gitlab **********'
pod install
